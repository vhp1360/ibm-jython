import sys

import re
if len(sys.argv) != 3:
  print "**************************************************************************************************************"
  print "*********   you should provide 3 input arguments:                                                       ******"
  print "**********   first : war file path                                                                      ******"
  print "**********   second Contecnt root like: ebanking                                                        ******"
  print "**********   thrid jndiFile path                                                                        ******"
  print "**********      if there is not any JNDI type none but if it has jndi, file format should be like below:******"
  print "**********      jdbc/AppJndiRequired:jdbc/jndiYouProvide                                                ******"
  print "**************************************************************************************************************"
  exit()
#----------------------------------------------
def getOptions():
  result = '[ -nopreCompileJSPs -distributeApp -nouseMetaDataFromBinary -createMBeansForResources -noreloadEnabled -nodeployws -validateinstall warn -noprocessEmbeddedConfig -filepermission .*\.dll=755#.*\.so=755#.*\.a=755#.*\.sl=755 -noallowDispatchRemoteInclude -noallowServiceRemoteInclude -asyncRequestDispatchType DISABLED -nouseAutoLink -noenableClientModule -clientMode isolated -novalidateSchema \
    -installed.ear.destination '+ appInfo["appAliasName"]+' \
    -appname '+ appInfo["appAliasName"]+' \
    -MapWebModToVH [[ '+ makeMapWebMod2VH()+' ]] \
    -MapModulesToServers [[ '+makeMapModule()+' ]]'
  if isCluster: result+=' -nodeployejb '
  if appInfo["ctxRoot"] != "":
    result+=makeContextRoot()
  if sys.argv[2] != 'none':
    result+=makeJndi()
  result+=']'
  return result
#end of getOptions======================================================================
def makeJndi():
  NameInModule=appInfo["ModuleName"]
  JndiIn={}
  JndiFile=open(sys.argv[2],"r")
#  jndiList=[]
#  jndiLst= AdminTask.listAuthDataEntries().split('\n')
#  for q in jndiLst : jndiList=q.split('] [')[1]
  result=" -MapResRefToEJB ["
  if version.split('.')[0] == '9' :
    for line in JndiFile: JndiIn[line.split(':')[0]]=line.split(':')[1].strip()
    for k,v in JndiIn.iteritems(): result+='[ '+NameInModule+' "" '+appInfo["appName"]+',WEB-INF/web.xml '+k+' javax.sql.DataSource '+v+' "" "" ""  ]'
  elif version.split('.')[0] == '8' :
    for line in JndiFile.readlines(): JndiIn[line.split(':')[0]]=line.split(':')[1].strip()
		for k,v in JndiIn.items(): result+='[ '+NameInModule+' "" '+appInfo["appName"]+',WEB-INF/web.xml '+k+' javax.sql.DataSource '+v+' "" "" ""  ]'
  result+=']'
  return result
#end of makeJndi========================================================================
def makeContextRoot():
	result=' -contextroot '+appInfo["ctxRoot"]+ ' -CtxRootForWebMod [['+appInfo["ModuleName"]+' '+appInfo["appName"]+',WEB-INF/web.xml '+appInfo["ctxRoot"]+' ]]'
  return result
#end of makeContextRoot================================================================
def makeMapModule():
  if isCluster:
    result=appInfo["ModuleName"]+' '+ appInfo["appName"] +',WEB-INF/web.xml WebSphere:cell='+_cellName_+',cluster=HC'
    for q in range(len(ihsList)): result+='+WebSphere:cell='+_cellName_+',node='+ihsFullList[q]+',server='+ihsList[q]
  elif isStandalone:
    result=appInfo["ModuleName"]+' '+ appInfo["appName"] +',WEB-INF/web.xml WebSphere:cell='+_cellName_+',node='+wasList[0]+',server='+nodeLst[0].split('(')[0]+'+WebSphere:cell='+_cellName_+',node='+wasList[0]+',server='+ihsList[0]
  return result
#end of makeMapModule==================================================================
def checkAutoStartState(enable="false"):
  AppList=AdminApp.list().split('\n')
  appAliasName=appInfo["appAliasName"].lower()
  for app in AppList:
    if appAliasName == app.lower() : continue
    elif AdminApp.view(app,'-CtxRootForWebMod').split('\n')[-1].split(' ')[-1].replace('/','') == appInfo["ctxRoot"]:
      deployments = AdminConfig.getid("/Deployment:"+app+"/")
      deploymentObj1 = AdminConfig.showAttribute(deployments, 'deployedObject')
      targetMap1 = AdminConfig.showAttribute(deploymentObj1, 'targetMappings')
      targetMap1 = targetMap1[1:len(targetMap1)-1].split(" ")
      for targetMapping in targetMap1:
        AdminConfig.modify(targetMapping, [["enable", enable]])
#end of checkAutoStartState============================================================
def getModuleName():
  result=appInfo["appAliasName"]
  Lines=AdminApp.taskInfo(appInfo["appPath"],'ModuleBuildID').split('\n')
  for line in Lines :
    if re.search('Module:',line) :
      result="'"+line.split(':')[1].strip()+"'"
      break
  return result
#end of getModuleName==================================================================
def makeMapWebMod2VH():
  result= appInfo["ModuleName"] +' '+appInfo["appName"]+',WEB-INF/web.xml default_host'
  return result
#end of makeMapWebMod2VH===============================================================
def checkState():
  state=AdminControl.getAttribute(cluster,'state')
  if version.split('.')[0]=='9':
    while 'partial' in state : state=AdminControl.getAttribute(cluster,'state')
  elif version.split('.')[0]=='8':
    while state.find('partial')>=0 : state=AdminControl.getAttribute(cluster,'state')
  return
#end of checkState=====================================================================
#======================================================================================
#======================================================================================
#-------------------------------------Required
appInfo={}
appInfo["appPath"]=sys.argv[0] #'HamahBankSMS-991227.war'
appInfo["ctxRoot"]=sys.argv[1] #'ContextRoot'
appInfo["appName"]=ntpath.basename(appInfo["appPath"])
appInfo["appAliasName"]=appInfo["appName"].replace(".war","_war")
appInfo["ModuleName"]=getModuleName()

#----------------------------------------------
#-----------------------------------------Pre Initial
cluster = AdminControl.completeObjectName('WebSphere:type=Cluster,*')
standalone = AdminControl.completeObjectName('WebSphere:type=Server,*')
isCluster=0
isStandalone=0
if re.search('type=Cluster',cluster):
    isCluster=1
    version=cluster.split(',')[4].split('=')[1]
elif re.search('type=Server',standalone):
    isStandalone=1
    version=standalone.split(',')[5].split('=')[1]
else :
  print 'Type of Service[Cluster or Standalone] could not be distinc'
  exit()
nodeLst=AdminTask.listServers('[-serverType APPLICATION_SERVER]').split('\n')
webLst=AdminTask.listServers('[-serverType WEB_SERVER ]').split('\n')
_cellName_=AdminControl.getCell()
wasList=[]
ihsList=[]
ihsFullList=[]
for q in nodeLst: wasList.append(q.split('/')[3])
for q in webLst: ihsList.append(q.split('/')[5].split('|')[0]);ihsFullList.append(q.split('/')[3])
AdminApp.install(appInfo["appPath"], getOptions())
checkAutoStartState()
AdminConfig.save()
if isCluster :
  AdminControl.invoke(cluster , 'stop')
  checkState()
  AdminControl.invoke(cluster , 'start')
  checkState()
elif isStandalone :
  AdminControl.invoke(standalone,'restart')
#  import os
#  os.system('kill -9 $(ps faux | grep java | grep ')
#    while 'partial' in state : state=AdminControl.getAttribute(cluster,'state')
print '=========================== install Completed=============================================='
