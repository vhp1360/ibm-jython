# -*- coding: utf-8 -*-
import sys
import re
#import json

path= '/home/Ansible/'
print "====================== بنام خدا =========================="
execfile(path+'global.py')
#===================================Functions==========================================================
def getWASType():
	return gWASType()
#------------------------------------------------------------------------------------------------------
wasType,version=getWASType()
""" يعني تمام DataSource ها"""
aDS=AdminConfig.list('DataSource').splitlines()
hDict={}
hostName=socket.gethostname()
dsDict={}
hCsv=''
hIP=gHostIP()
dsDict["hIP"]=hIP
for DS in aDS:
	dataSourceName=AdminConfig.showAttribute(DS,'name')
	proDict={}
	"""DataSource Name"""
	proDict["jndiName"]=AdminConfig.showAttribute(DS,'jndiName')
	UserAlias=AdminConfig.showAttribute(DS,'authDataAlias')
	if UserAlias is None: UserAlias=''
	"""Properties For DataSources"""
	dsProp=AdminConfig.list("J2EEResourceProperty",DS).split('\n')
	"""find which properties we need in Properties Collection"""
	for pro in dsProp:
		if pro.find('databaseName')>=0:
			dbName=AdminConfig.showAttribute(pro,'value')
			if not dbName.find('${USER_INSTALL_ROOT}')>=0:
				proDict["dbName"]=dbName
			else:
				continue
		elif pro.find('serverName')>=0:
			IP=pro
			proDict["dbIP"]=AdminConfig.showAttribute(pro,'value')
	"""Java User Authentication Info"""
	JA=AdminConfig.list("JAASAuthData").splitlines()
	"""find related User to DataSource"""
	for q in JA:
		if AdminConfig.showAttribute(q,'alias').find(UserAlias)>=0:
			Ja=q
			break
	proDict["userAlias"]=UserAlias
	proDict["userName"]=AdminConfig.showAttribute(Ja,'userId')
	dsDict[dataSourceName]=proDict
	hCsv=hCsv+hostName+','+hIP+','+','.join(proDict.values())+'\n'
hDict[hostName]=dsDict
#print json.dumps(hDict)
print '*********************************************************************************'
print hCsv
print '#################################################################################'
f=open('/tmp/%s.JNDIdata.json' % hIP ,'w')
f.write(str(hDict))
f.close()
f=open('/tmp/%s.JNDIdata.csv' % hIP ,'w')
f.write(hCsv)
f.close()
