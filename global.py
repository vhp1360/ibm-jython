import sys
import os
import re
import socket

#==========WebSphere Type===================
def gWASType():
	cluster = AdminControl.completeObjectName('WebSphere:type=Cluster,*')
	standalone = AdminControl.completeObjectName('WebSphere:type=Server,*')
	wasType=''
	version=''
	if re.search('type=Cluster',cluster):
		wasType='Cluster'
		version=cluster.split(',')[4].split('=')[1]
	elif re.search('type=Server',standalone):
		wasType='Standalone'
		version=standalone.split(',')[5].split('=')[1]
	else :
		print 'Type of Service[Cluster or Standalone] could not be distinc'
		wasType='None'
	return wasType,version
##############################################
#==========WAS Primary User Name===================
def gWASPrimaryUser():
	AdminName={}
	UserInfo=AdminTask.getUserRegistryInfo()
	for q in re.findall(r'\w+\s{1}\w*',UserInfo):
		k,v=q.split(' ')
		AdminName[k]=v
	return AdminName["primaryAdminId"]

##############################################
#==========Find IP ===========================
def gHostIP():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("172.16.44.4", 80))
	ip=s.getsockname()[0]
	s.close()
	if not ip.find('172.')>=0:
		s=socket.gethostbyname(socket.gethostname())
		ip=str(s)
	if not ip.find('172.')>=0:
		ip=str(os.system("hostname -i"))

	return ip
##############################################
